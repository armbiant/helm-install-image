# Helm install image

Minimal images with [Helm](https://helm.sh/) and [kubectl](https://kubernetes.io/docs/reference/kubectl/) installed. Built for GitLab's cluster integration.

There are three variants:


## Helm v3

Contains Helm 3 and kubectl

### Binaries

- Helm v3:
  - `/usr/bin/helm`
- kubectl:
  - `/usr/bin/kubectl`

### Image

```plaintext
registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image:3.16.4-kube-1.31.4-alpine-3.21
```

### Tag strategy for Docker image

We tag two release images. Go to [tag_strategy.md](doc/tag_strategy.md) for more details.
